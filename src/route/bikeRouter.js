import { Router } from "express"


export let bikeRouter = Router()
bikeRouter
  .route("/")
  .post((req,res,next)=>{
    console.log("I am normal middleware 1")
    let error = new Error("this is my 2nd error")
    next(error)
  },
  (error,req,res,next)=>{
    console.log("I am error middleware 1") 
    console.log(error.message) 
    
  },
  (req,res,next)=>{
    console.log("I am normal middleware 2")
    next()
  },
  (error,req,res,next)=>{
    console.log("I am error middleware 2")
  },
  (req,res,next)=>{
    console.log("I am normal middleware 3")
    res.json({
      success:true,
      message:"bike created successfully"


    })})
  .get((req,res,next)=>{
    console.log(req.body)
    res.json("bike get")
  }) 
  .patch((req,res,next)=>{
    res.json("bike update")
  })
  .delete((req,res,next)=>{
    res.json("bike delete")
  })
  bikeRouter
  .route("/name")//localhost:8000/bike/name
  .get((req,res,next)=>{
    console.log(req.body) 
    res.json("bike name get")

  })
  bikeRouter
  .route("/:id")//localhost:8000/bike/:id
  .get((req,res,next)=>{
    console.log(req.params)
    /*
    req.params={
      id:1234
    }
     */
    /*
    req.query={
      name="lx"
      address="bkt"
    }
     */
    console.log(req.query)
    res.json(req.query)
  })
  bikeRouter
  .route("/:id1/name/:id2")
     .get((req,res,next)=>{
      console.log(req.params)
      res.json(req.params)
      /*
      id1:"jjj"
      id2:"123"
       */
     })