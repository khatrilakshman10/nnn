import { Router } from "express";
import { Classroom } from "../schema/model.js";

let classroomRouter=Router()
classroomRouter.route("/").post(async(req,res,next)=>{
    let data=req.body
    try { let result=await  Classroom.create(data)
        res.json({
            success:true,
            message:"Classroom create successfully",
            result:result
        })
        
    } catch (error) {
         res.json({
        success:false,
        message:error.message,
    })
        
    }
 
}) .get(async(req,res,next)=>{
    try { let result=await Classroom.find({})
    res.json({
        success:true,
        message:"Classroom read successfully",
        result:result
    })
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })
        
    }
   
}) 
classroomRouter
.route("/:id")
.get(async(req,res,next)=>{
   try {
    let result=await Classroom.findById(req.params.id)
    res.json({
        success:true,
        message:"Classroom read successfully",
        result:result
    
   })} catch (error) {
    res.json({
        success:false,
        message:error.message,
    })   
   }
})
classroomRouter
.route("/:id")
.patch(async(req,res,next)=>{
   try {
    let result=await Classroom.findByIdAndUpdate(req.params.id,req.body,{
        new:true,})
    res.json({
        success:true,
        message:"Classroom update successfully",
        result:result,
    
   })} catch (error) {
    res.json({
        success:false,
        message:error.message,
    })   
   }
})
classroomRouter
.route("/:id")
.delete(async(req,res,next)=>{
   try {
    let result=await Classroom.findByIdAndDelete(req.params.id)
    res.json({
        success:true,
        message:"Classroom deleted successfully",
        result:result,
    
   })} catch (error) {
    res.json({
        success:false,
        message:error.message,
    })   
   }
})
export default classroomRouter