import { Router } from "express";
import { Book } from "../schema/model.js";

let bookRouter=Router()
bookRouter.route("/").post(async(req,res,next)=>{
    let data=req.body
    try{
   let result=await Book.create(data)
    res.json({
        success:true,
        message:"book create successfully",
        result:result
    })}catch(error){
        res.json({
            success:false,
            message:error.message,
        })
    }
}) .get(async(req,res,next)=>{
    let result=await Book.find({})
    res.json({
        success:true,
        message:"Book read successfully",
        result:result
    })
})
bookRouter
.route("/:id")
.get(async(req,res,next)=>{
   try {
    let result=await Book.findById(req.params.id)
    res.json({
        success:true,
        message:"Book read successfully",
        result:result
    
   })} catch (error) {
    res.json({
        success:false,
        message:error.message,
    })   
   }
})
bookRouter
.route("/:id")
.patch(async(req,res,next)=>{
   try {
    let result=await Book.findByIdAndUpdate(req.params.id,req.body,{
        new:true,})
    res.json({
        success:true,
        message:"Book update successfully",
        result:result,
    
   })} catch (error) {
    res.json({
        success:false,
        message:error.message,
    })   
   }
})
bookRouter
.route("/:id")
.delete(async(req,res,next)=>{
   try {
    let result=await Book.findByIdAndDelete(req.params.id)
    res.json({
        success:true,
        message:"Book deleted successfully",
        result:result,
    
   })} catch (error) {
    res.json({
        success:false,
        message:error.message,
    })   
   }
})
export default bookRouter