import { Router } from "express";
import { createStudent, deleteSpecificStudent, readSpecificStudent, readStudent, updateSpecificStudent } from "../controller/studentController.js";

export let studentRouter=Router()
studentRouter.route("/")
.post(createStudent)
.get(readStudent)
studentRouter
.route("/:id")
.get(readSpecificStudent)
studentRouter
.route("/:id")
.patch(updateSpecificStudent)
studentRouter
.route("/:id")
.delete(deleteSpecificStudent)
export default studentRouter