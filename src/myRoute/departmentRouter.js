import { Router } from "express";
import { Department } from "../schema/model.js";

let departmentRouter=Router()
departmentRouter.route("/").post(async(req,res,next)=>{
    let data=req.body
    try {
        let result=await Department.create(data)
        res.json({
            success:true,
            message:"Department create successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })
        
    }
  
   
}) .get(async(req,res,next)=>{
    try {
    let result=await Department.find({})
    res.json({
        success:true,
        message:"Department read successfully",
        result:result
    })  
    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })
        
    }
  
})
departmentRouter
.route("/:id")
.get(async(req,res,next)=>{
   try {
    let result=await Department.findById(req.params.id)
    res.json({
        success:true,
        message:"Department read successfully",
        result:result
    
   })} catch (error) {
    res.json({
        success:false,
        message:error.message,
    })   
   }
})
departmentRouter
.route("/:id")
.patch(async(req,res,next)=>{
   try {
    let result=await Department.findByIdAndUpdate(req.params.id,req.body,{
        new:true,})
    res.json({
        success:true,
        message:"Department update successfully",
        result:result,
    
   })} catch (error) {
    res.json({
        success:false,
        message:error.message,
    })   
   }
})
departmentRouter
.route("/:id")
.delete(async(req,res,next)=>{
   try {
    let result=await Department.findByIdAndDelete(req.params.id)
    res.json({
        success:true,
        message:"Department deleted successfully",
        result:result,
    
   })} catch (error) {
    res.json({
        success:false,
        message:error.message,
    })   
   }
})
export default departmentRouter