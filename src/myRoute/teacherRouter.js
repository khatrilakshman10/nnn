import { Router, json } from "express";
import { Teacher } from "../schema/model.js";


let teacherRouter=Router()
teacherRouter.route("/").post(async(req,res,next)=>{
    let data=req.body
    try {
        let result=await Teacher.create(data)
        res.json({
            success:true,
            message:"Teacher create successfully",
            result:result
        })  
    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })     
    }
  
    
}).get(async(req,res,next)=>{
    try {
        let result=await Teacher.find({})
        res.json({
            success:true,
            message:"Teacher read successfully",
            result:result
        })  
    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })     
    }
   
})
teacherRouter
.route("/:id")
.get(async(req,res,next)=>{
   try {
    let result=await Teacher.findById(req.params.id)
    res.json({
        success:true,
        message:"Teacher read successfully",
        result:result
    
   })} catch (error) {
    res.json({
        success:false,
        message:error.message,
    })   
   }
})
teacherRouter
.route("/:id")
.patch(async(req,res,next)=>{
   try {
    let result=await Teacher.findByIdAndUpdate(req.params.id,req.body,{
        new:true,})
    res.json({
        success:true,
        message:"Teacher update successfully",
        result:result,
    
   })} catch (error) {
    res.json({
        success:false,
        message:error.message,
    })   
   }
})
teacherRouter
.route("/:id")
.delete(async(req,res,next)=>{
   try {
    let result=await Teacher.findByIdAndDelete(req.params.id)
    res.json({
        success:true,
        message:"Teacher deleted successfully",
        result:result,
    
   })} catch (error) {
    res.json({
        success:false,
        message:error.message,
    })   
   }
})
export default teacherRouter