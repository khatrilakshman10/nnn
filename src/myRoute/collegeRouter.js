import { Router } from "express";
import { College } from "../schema/model.js";

let collegeRouter=Router()
collegeRouter.route("/").post(async(req,res,next)=>{
    let data=req.body
    try {
        let result=await College.create(data)
        res.json({
            success:true,
            message:"College create successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })
        
    }
  
    
    
}).get(async(req,res,next)=>{
    try {
        let result=await College.find({})
        res.json({
            success:true,
            message:"College read successfully",
            result:result
        })
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })
        
    }
  
})
collegeRouter
.route("/:id")
.get(async(req,res,next)=>{
   try {
    let result=await College.findById(req.params.id)
    res.json({
        success:true,
        message:"College read successfully",
        result:result
    
   })} catch (error) {
    res.json({
        success:false,
        message:error.message,
    })   
   }
})
collegeRouter
.route("/:id")
.patch(async(req,res,next)=>{
   try {
    let result=await College.findByIdAndUpdate(req.params.id,req.body,{
        new:true,})
    res.json({
        success:true,
        message:"College update successfully",
        result:result,
    
   })} catch (error) {
    res.json({
        success:false,
        message:error.message,
    })   
   }
})
collegeRouter
.route("/:id")
.delete(async(req,res,next)=>{
   try {
    let result=await College.findByIdAndDelete(req.params.id)
    res.json({
        success:true,
        message:"College deleted successfully",
        result:result,
    
   })} catch (error) {
    res.json({
        success:false,
        message:error.message,
    })   
   }
})

export default collegeRouter