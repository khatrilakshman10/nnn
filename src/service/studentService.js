import { Student } from "../schema/model.js"

export let createStudentService=async(data)=>{
    return await Student.create(data)
}
export let readStudentService=async()=>{
    return await Student.find({})
}
export let readSpecificStudentService=async (id)=>{
   return await Student.findById(id)
}
export let updateSpecificStudentService=async(id,data)=>{
    return await Student.findByIdAndUpdate(id,data,{new:true})
}
export let deleteStudentServiceService = async (id) => {
    return await Student.findByIdAndDelete(id);
  };