import { Student } from "../schema/model.js";
import {
  createStudentService,
  deleteStudentServiceService,
  readSpecificStudentService,
  readStudentService,
  updateSpecificStudentService,
} from "../service/studentService.js";

export let createStudent = async (req, res, next) => {
  let data = req.body;
  try {
    let result = await createStudentService(req.body);
    res.json({
      success: true,
      message: "Student created successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export let readStudent = async (req, res, next) => {
  try {
    let result = await readStudentService();
    res.json({
      success: true,
      message: "Student read successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export let readSpecificStudent = async (req, res, next) => {
  try {
    let result = await readSpecificStudentService(req.params.id);
    res.json({
      success: true,
      message: "Student read successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export let updateSpecificStudent = async (req, res, next) => {
  try {
    let result = await updateSpecificStudentService(req.params.id, req.body, {
      new: true,
    });
    res.json({
      success: true,
      message: "Student update successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export let deleteSpecificStudent = async (req, res, next) => {
  try {
    let result = await deleteStudentServiceService(req.params.id);
    req.params.id;
    res.json({
      success: true,
      message: "Student deleted successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
